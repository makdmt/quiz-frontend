/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'export',
    distDir: 'dist',
    basePath: '/quiz',
    typescript: {
        ignoreBuildErrors: true
    }
};

export default nextConfig;
