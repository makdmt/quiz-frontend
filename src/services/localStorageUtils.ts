export const getArrFromLocalStorage = async (arrKey: string) => {
    let stringifiedArr = localStorage.getItem(arrKey);
    let arr = [];
    if (stringifiedArr) {
        arr = await JSON.parse(stringifiedArr);
    }
    return arr;
}

export const pushToLocalStorageArr = async (arrKey: string, val: string) => {
    let arr = await getArrFromLocalStorage(arrKey);

    const set = new Set(arr);
    set.add(val);
    arr = Array.from(set);
    localStorage.setItem(arrKey, await JSON.stringify(arr));
} 