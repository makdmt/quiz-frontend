const url = 'http://localhost:4200';

export const postRetry = retry(postData, 3, 500);

async function postData(data: any) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });

    if (!response.ok) {
        throw new Error('failed POST');
    }

    return await response.json();
}

function retry(fn: Function, attempts = 3, delay = 1000) {
    return async function (...args: any[]) {
        for (let i = 0; i < attempts; i++) {
            try {
                await fn(...args);
            } catch (err) {
                if (attempts > 0) await new Promise(resolve => setTimeout(resolve, delay));
                if (i === attempts - 1) throw (err);
            }
        }
    }
}