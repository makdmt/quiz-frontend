export const RIGHT_ANSWER_COST = 50;
export const WRONG_ANSWER_COST = -15;

export const calcScores = (rightAnswers: string[], wrongAnswers: string[]) => {

    const res = rightAnswers.length * RIGHT_ANSWER_COST + wrongAnswers.length * WRONG_ANSWER_COST;
    return res > 0 ? res : 0
}