import { FormEvent, useState } from "react";
import { Button } from "../Button/Button";
import { useForm } from "@/hooks/useForm";

import styles from './Login.module.css';

type LoginProps = {
    setUsername: Function;
}

export const LoginComponent = ({ setUsername }: LoginProps) => {

    const [isSubmited, setSubmited] = useState<boolean>(false);
    const { values: { username }, onChange } = useForm({ username: '' });

    const onSubmit = (evt: FormEvent) => {
        evt.preventDefault();
        setSubmited(true);

        setTimeout(() => {
            setUsername(username);
        }, 1500)

    }

    if (isSubmited) return <p className={styles.container}>{`Приятно познакомиться, ${username}!`}</p>

    return (
        <form name="register" onSubmit={onSubmit} className={styles.container}>
            <h1>Как тебя зовут?</h1>
            <input type="text" name="username" onChange={onChange} placeholder="Твое имя" minLength={2} maxLength={15} className={styles.input}></input>
            <Button type="submit" disabled={username?.length < 2}>Отправить!</Button>
        </form>
    )
}