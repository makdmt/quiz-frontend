'use client'
import { MouseEventHandler, useCallback, useEffect, useMemo, useState } from 'react';
import styles from './Field.module.css'
import { Cell, generateCells } from '@/services/cellGenerator';
import { CellComponent } from '../Cell/Cell';

interface FieldProps {
    fieldHeader: string;
    cells: Cell[] | undefined;
    cellsToSelectCount: number;
    updateCells: (cells: Cell[]) => void
}

export const Field = ({ fieldHeader, cells, cellsToSelectCount, updateCells }: FieldProps) => {

    const selectedCellsCount = useMemo(() => cells?.reduce((acc, cell) => cell.isActive ? acc += 1 : acc, 0) || 0, [cells])

    const toggleCellState = (cell: Cell) => {
        if (!cell.isActive && selectedCellsCount < cellsToSelectCount) {
            cell.isActive = true;
        } else {
            cell.isActive = false;
        }
        if (cells) updateCells([...cells]);
    }

    return (
        <div>
            <h2 className={styles.heading}>{fieldHeader}</h2>
            <span className={styles.subHeading}>Отметьте {cellsToSelectCount}</span>
            <ul className={styles.cellsContainer}>
                {cells && cells?.length > 0 && cells.map((cell, index) => {
                    return (
                        <CellComponent key={index} cell={cell} toggleCellState={toggleCellState}></CellComponent>
                    )
                })}
            </ul>
        </div>
    )

}