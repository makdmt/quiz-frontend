'use client'

import styles from "./Quiz.module.css"
import { AnswerButton } from "../Button/AnswerButton"
import { useContext, useMemo, useState } from "react"
import { usePathname } from "next/navigation"
import { NavigateButton } from "../Button/NavigateButton"
import { TickIcon } from "../Icons/Tick"
import { CrossIcon } from "../Icons/Cross"
import { TryAgainButton } from "../Button/TryAgainButton"
import { pushToLocalStorageArr } from "@/services/localStorageUtils"
import { ScoresContext } from "@/app/layout"
import { RIGHT_ANSWER_COST, WRONG_ANSWER_COST } from "@/services/scoresBilling"

export type QuizProps = {
    question: string,
    imgUrl: string,
    rightAnswer: string,
    wrongAnswers: string[],
    successRedirectURL: string
}

export const QuizComponent = ({ question, imgUrl, rightAnswer, wrongAnswers, successRedirectURL }: QuizProps) => {

    const { recalcScores, sendAnswer } = useContext(ScoresContext);
    const path = usePathname();

    const [selectedAnswer, setSelectedAnswer] = useState<string>()

    const buttons: string[] = useMemo(() => {
        const answers = [...wrongAnswers];
        answers.push(rightAnswer);
        return answers.sort(() => Math.random() - 0.5)
    }, [rightAnswer, wrongAnswers])

    const saveAnswer = async (answer: string) => {
        if (answer === rightAnswer) {
            await pushToLocalStorageArr('rightAnswers', `${path.slice(1)}_${answer}`);
        } else {
            await pushToLocalStorageArr('wrongAnswers', `${path.slice(1)}_${answer}`);
        }
        recalcScores && recalcScores();
        setSelectedAnswer(answer);
        sendAnswer(
            parseInt(path.slice(1)),
            answer,
            answer === rightAnswer ? RIGHT_ANSWER_COST : WRONG_ANSWER_COST
        )
    }


    if (selectedAnswer === rightAnswer) return (
        <>
            <div className={styles.tickIcon} >
                <TickIcon />
            </div>
            <p className={styles.replyComment}>Молодец!</p>
            <NavigateButton redirectUrl={successRedirectURL} />
        </>
    )

    if (selectedAnswer && selectedAnswer !== rightAnswer) return (
        <>
            <div className={styles.crossIcon} >
                <CrossIcon />
            </div>
            <p className={styles.replyComment}>Увы, нет...</p>
            <TryAgainButton onClick={() => setSelectedAnswer(undefined)} />
        </>
    )


    return (
        <div className={styles.container}>
            <div className={styles.imageContainer}>
                <img src={imgUrl} className={styles.image} />
            </div>
            <h3 className={styles.heading}>{question}</h3>
            <div className={styles.buttonsContainer}>
                {buttons.map((button: string, index: number) => {
                    return (
                        <AnswerButton key={index} answerText={button} selectAnswer={saveAnswer} />
                    )
                })}
            </div>
        </div>
    )

}