import { HTMLProps } from "react"
import styles from './Button.module.css'


export const TryAgainButton = ({ onClick, disabled }: HTMLProps<HTMLButtonElement> ) => {


    return (
        <button type="button" onClick={onClick} disabled={disabled} className={`${styles.btn} ${styles.tryAgaingBtn}`}>
            Попробуй еще раз!
        </button>
    )
}