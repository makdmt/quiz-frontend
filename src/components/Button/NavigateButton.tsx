import { HTMLProps } from "react"
import styles from './Button.module.css'
import Link from "next/link";

type Props = {
    redirectUrl: string;
}

export const NavigateButton = ({ redirectUrl }: HTMLProps<HTMLButtonElement> & Props) => {


    return (
        <Link href={redirectUrl} className={styles.navigateBtn}>
            <button className={`${styles.btn}`}>Поехали дальше!</button>
        </Link>
    )
}