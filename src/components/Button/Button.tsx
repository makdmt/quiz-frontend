import { HTMLProps } from "react"
import styles from './Button.module.css'

type Props = {
    type: "submit" | "button" | "reset";
}


export const Button = (props: HTMLProps<HTMLButtonElement> & Props) => {

    return (
        <button {...props} className={styles.btn}>
            {props.children}
        </button>
    )
}