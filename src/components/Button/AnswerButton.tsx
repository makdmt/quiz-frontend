import { HTMLProps } from "react"
import styles from './Button.module.css'

type Props = {
    selectAnswer: Function
    answerText: string;
}

export const AnswerButton = ({ selectAnswer, disabled, answerText }: HTMLProps<HTMLButtonElement> & Props) => {

    const onClick = () => {
        selectAnswer(answerText);
    }
    return (
        <button type="button" onClick={onClick} disabled={disabled} className={`${styles.btn} ${styles.answerBtn}`}>
            {answerText}
        </button>
    )
}