
import { Cell } from '@/services/cellGenerator'
import styles from './Cell.module.css'
import { HTMLProps, MouseEventHandler } from 'react'

export const CellComponent = ({ cell, toggleCellState }: { cell: Cell } & { toggleCellState: Function }) => {

    const clickHandler: MouseEventHandler<HTMLButtonElement> = (evt) => {
        evt.stopPropagation();
        toggleCellState(cell);
    }

    return (
        <button
            className={`${styles.cell} ${cell?.isActive && styles.active}`}
            onClick={clickHandler}
        >
            {cell?.num}
        </button>
    )
}