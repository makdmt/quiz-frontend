'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Какой танец часто называют 'танцем королей'?!!",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Венский вальс",
    wrongAnswers: ["Фокстрот", "Танго", "Полонез"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.409145%2C55.779702&whatshere%5Bzoom%5D=17.655128&ll=37.409622050849094%2C55.77951659950685&z=17.655128"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
