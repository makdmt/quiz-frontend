'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Если ты разделишь число 120 на четверть его значения, какой результат получишь?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "4",
    wrongAnswers: ["10", "30", "40"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.416506%2C55.779483&whatshere%5Bzoom%5D=18.0&ll=37.41520653540292%2C55.77929884089851&z=18.0"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
