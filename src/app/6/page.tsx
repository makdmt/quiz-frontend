'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Что такое 'двойной дриблинг' в баскетболе?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Когда игрок начинает ведение мяча, останавливается и начинает снова без передачи",
    wrongAnswers: ["Когда игрок ведет мяч обеими руками одновременно", "Два последовательных броска в корзину", "Пас между двумя игроками без приземления мяча"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.417506%2C55.777413&whatshere%5Bzoom%5D=17.227364&ll=37.4174112353837%2C55.77754300537723&z=17.227364"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
