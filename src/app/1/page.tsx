'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Исследователи выявили, что ДНК человека на 50% совпадают с ДНК этого фрукта. Что это за фрукт?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Банан",
    wrongAnswers: ["Яблоко", "Киви", "Авокадо"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.427827%2C55.775772&whatshere%5Bzoom%5D=17.08065&ll=37.425864299459455%2C55.776020919400395&z=17.08065"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
