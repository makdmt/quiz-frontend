'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Какой из спутников Нептуна самый большой?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Тритон",
    wrongAnswers: ["Нереида", "Протей", "Ларисса"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.406256%2C55.782967&whatshere%5Bzoom%5D=18.769747&ll=37.40624065651468%2C55.782850324322965&z=18.769747"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
