'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Чему равна идеальная длина горных лыж для ребенка?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "На 10-15 см ниже роста",
    wrongAnswers: ["На уровне роста", "На 10-15 см выше роста", "На уровне подбородка"],
    successRedirectURL: "https://yandex.ru/maps/-/CDVku8mA"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
