'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";


const quizData: QuizProps = {
  question: "Что это за рыба?",
  imgUrl: "https://fishingby.com/wp-content/uploads/2021/09/lin-2.jpg",
  rightAnswer: "Линь",
  wrongAnswers: ["Щука", "Карась", "Палтус"],
  successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.409145%2C55.779702&whatshere%5Bzoom%5D=17.655128&ll=37.409622050849094%2C55.77951659950685&z=17.655128"

}


export default function Home() {

  const pathname = usePathname();

  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
