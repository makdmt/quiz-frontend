'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Какой из этих предмет необходим для ориентирования в неизвестной местности?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Компас",
    wrongAnswers: ["Фонарик", "Зонтик", "Скакалка"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.434786%2C55.777611&whatshere%5Bzoom%5D=20.448887&ll=37.43474922904739%2C55.7775992091978&z=20.448887"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
