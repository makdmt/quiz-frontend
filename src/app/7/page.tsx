'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Что является необходимым атрибутом для всех видов художественной гимнастики?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Музыка",
    wrongAnswers: ["Коврик", "Лента", "Обруч"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.425221%2C55.779044&whatshere%5Bzoom%5D=18.938671&ll=37.42525773020048%2C55.77887709778846&z=18.938671"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
