'use client'

import { createContext, useEffect, useState } from "react";
import { Inter } from "next/font/google";
import "./globals.css";
import styles from "./layout.module.css"
import { LoginComponent } from "@/components/Login/Login";
import { calcScores } from "@/services/scoresBilling";
import { getArrFromLocalStorage } from "@/services/localStorageUtils";

const inter = Inter({ subsets: ["latin"] });

const apiURL = 'https://api.dmt.nomoredomainswork.ru/quiz/answers'

type ScoresState = {
  username?: string | null;
  recalcScores: Function;
  sendAnswer: (questionNumber: number, answer: string, answerCost: number) => void
}

export const ScoresContext = createContext<ScoresState>({});

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {

  const [loading, setLoading] = useState(true)
  const [username, setUsername] = useState<ScoresState['username']>(null)
  const [scores, setScores] = useState<number>(-1);


  useEffect(() => {
    setUsername(localStorage.getItem('username'));
    setLoading(false);
  }, [])


  useEffect(() => {
    const getAnswers = async () => {
      const rightAnswers = await getArrFromLocalStorage('rightAnswers');
      const wrongAnswers = await getArrFromLocalStorage('wrongAnswers');
      const scores = calcScores(rightAnswers, wrongAnswers);
      setScores(scores);
    }
    getAnswers();
  }, [scores])

  const login = (username: string) => {
    localStorage.setItem('username', username);
    setUsername(username);
  }

  const sendAnswer = (questionNumber: number, answer: string, answerCost: number) => {
    fetch(apiURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        questionNumber,
        answer,
        answerCost
      })

    })
  }

  return (
    <html lang="ru">
      <head>
        <title>Велоквест</title>
      </head>
      <body className={inter.className}>
        <ScoresContext.Provider value={{ recalcScores: () => setScores(-1), sendAnswer }}>
          <main className={styles.main}>
            {loading && <p className={styles.loader}>Загрузка...</p>}
            {!loading && (!username
              ? <LoginComponent setUsername={login} />
              : children
            )}
            {username && scores >= 0 && <footer className={styles.footer}>
              <h4 className={styles.scores}>{`Твои баллы: ${scores}`}</h4>
            </footer>
            }
          </main>
        </ScoresContext.Provider>
      </body>
    </html>
  );
}
