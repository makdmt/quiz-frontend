'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Что произойдет с байдаркой, если грести только с одной стороны?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Начнет кружиться",
    wrongAnswers: ["Пойдет прямо", "Ускорится", "Перевернется"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.433561%2C55.780063&whatshere%5Bzoom%5D=17.0&ll=37.434865094436994%2C55.77879136587291&z=17.0"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
