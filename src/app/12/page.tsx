'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";


const quizData: QuizProps = {
  question: "Что это за птица",
  imgUrl: "https://proza.ru/pics/2019/01/10/1359.jpg",
  rightAnswer: "Малиновка",
  wrongAnswers: ["Снегирь", "Голубь", "Страус"],
  successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.408548%2C55.785661&whatshere%5Bzoom%5D=17.0&ll=37.408547999999996%2C55.78566099961556&z=17.0"
}


export default function Home() {

  const pathname = usePathname();

  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
