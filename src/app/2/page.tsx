'use client'

import { QuizComponent, QuizProps } from "@/components/Quiz/Quiz";
import styles from "./page.module.css";
import { usePathname } from "next/navigation";
import { useMemo } from "react";


export default function Home() {

  const pathname = usePathname();

  const quizData: QuizProps = useMemo(() => ({
    question: "Как называется дом, в который был отнесен Гарри Поттер в Хогвартсе?",
    imgUrl: `/quiz/images/${pathname.slice(1)}.jpg`,
    rightAnswer: "Гриффиндор",
    wrongAnswers: ["Слизерин", "Равенкло", "Хаффлпафф"],
    successRedirectURL: "https://yandex.ru/maps?whatshere%5Bpoint%5D=37.42373138873541%2C55.781356941526965&whatshere%5Bzoom%5D=20.81874&ll=37.4237399376404%2C55.78137760240803&z=20.81874"
  }), [])


  return (
    <>
      <header className={styles.header}>
        <h1 className={styles.heading}>{`Вопрос ${pathname.slice(1)}`}</h1>
      </header>
      <QuizComponent {...quizData} />
    </>
  );
}
